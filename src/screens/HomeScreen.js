import React from 'react';
import { Text, View, StyleSheet, Image, ScrollView, TouchableHighlight, TextInput, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import {
    StackNavigator,
} from 'react-navigation';
import Expo from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { Container, Content, Header, Left, Icon } from 'native-base';
import Modal from "react-native-modal";

class HomeScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerLeft: (
            <Icon name="md-menu" size={28} style={{paddingLeft: 20, color: 'white', width:80}}
                  onPress={() => navigation.navigate('DrawerOpen')}/>
        ),
        headerStyle: {backgroundColor: '#0f70b7'},
        headerTitle: (
            <Image style={{alignItems:'center',marginLeft: 60}} source={require('../img/logo-neg.png')}/>
        ),
    });

    constructor(props) {
        super(props)
        this.state = {count: 0}
    }

    quantityUp = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    quantityDown = () => {
        this.setState({
            count: this.state.count - 1
        })
    }

    state = {
        isModalVisible: false
    };

    _toggleModal = () =>
        this.setState({isModalVisible: !this.state.isModalVisible});

    render() {
        const {navigate} = this.props.navigation;
        return (
            <KeyboardAvoidingView style={{flex:1}} behavior="padding" keyboardVerticalOffset={100}>
                <ScrollView>
                    <View style={styles.container}>
                        <Text style={styles.title}>Categories</Text>
                        <View style={styles.categoriesContainer}>
                            <View style={styles.categories}>
                                <Image style={{alignItems:'center'}} source={require('../img/category_image.png')}/>
                                <Text style={styles.categoryTitle}>
                                    CartQue
                                </Text>
                                <Text style={styles.queuesAvailable}>
                                    Queues 2
                                </Text>
                            </View>
                            <View style={styles.categories}>
                                <Image style={{alignItems:'center'}} source={require('../img/category_image.png')}/>
                                <Text style={styles.categoryTitle}>
                                    CartQue
                                </Text>
                                <Text style={styles.queuesAvailable}>
                                    Queues 2
                                </Text>
                            </View>
                        </View>
                        <View style={styles.categoriesContainer}>
                            <View style={styles.categories}>
                                <Image style={{alignItems:'center'}} source={require('../img/category_image.png')}/>
                                <Text style={styles.categoryTitle}>
                                    CartQue
                                </Text>
                                <Text style={styles.queuesAvailable}>
                                    Queues 2
                                </Text>
                            </View>
                            <View style={styles.categories}>
                                <Image style={{alignItems:'center'}} source={require('../img/category_image.png')}/>
                                <Text style={styles.categoryTitle}>
                                    CartQue
                                </Text>
                                <Text style={styles.queuesAvailable}>
                                    Queues 2
                                </Text>
                            </View>
                        </View>
                        <Text style={styles.title}>Products</Text>
                        <View style={styles.queueContainer}>
                            <TouchableHighlight onPress={() => navigate('QueueScreen')}>
                                <Image style={{alignItems:'center'}} source={require('../img/queue_image.png')}/>
                            </TouchableHighlight>
                            <View style={styles.titleContainer}>
                                <Text style={styles.queueTitle}>
                                    Single-Drug Urine Test Panel (CD-DBU-114)
                                </Text>
                                <View style={styles.queueQuantity}>
                                    <TouchableHighlight
                                        style={{borderColor: '#dee1e4', borderRightWidth:1,width:25, justifyContent: 'center', alignItems:'center'}}
                                        onPress={this.quantityUp}>
                                        <Text style={{color:'rgb(125, 125, 125)'}}>
                                            +
                                        </Text>
                                    </TouchableHighlight>
                                    <View style={styles.queueQuantityInput}>
                                        <TextInput underlineColorAndroid='transparent' placeholder="0" keyboardType='numeric'>
                                                <Text>
                                                    { this.state.count !== 0 ? this.state.count : null}
                                                </Text>
                                        </TextInput>
                                    </View>
                                    <TouchableHighlight
                                        style={{borderColor: '#dee1e4', borderLeftWidth:1, width:25, justifyContent: 'center', alignItems:'center'}}
                                        onPress={this.quantityDown}>
                                        <Text style={{color:'rgb(125, 125, 125)'}}>
                                            -
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                            </View>
                            <View style={styles.loyaltyPointsContainer}>
                                <Text style={styles.loyaltyPointsText}>
                                    You can earn 258 loyalty points per piece.
                                </Text>
                            </View>
                            <TouchableHighlight style={styles.button} onPress={this._toggleModal}>
                                <Text style={styles.buttonText}>
                                    <Image style={{margin:5, top:10}} source={require('../img/ic_add_shopping_cart_white_24dp.png')}/>
                                    BUY IT NOW
                                </Text>
                            </TouchableHighlight>
                            <View style={styles.queueFooter}>
                                <Text style={{bottom:13,color: '#a0a0a0',}}>
                                    Add to favorites
                                </Text>
                                <Image style={{alignItems:'center',marginLeft: 5,bottom:8,marginRight: 10,}}
                                       source={require('../img/ic_favorite_black_24dp.png')}/>
                            </View>
                        </View>
                        <View style={styles.queueContainer}>
                            <TouchableHighlight onPress={() => navigate('QueueScreen')}>
                                <Image style={{alignItems:'center'}} source={require('../img/queue_image.png')}/>
                            </TouchableHighlight>
                            <View style={styles.titleContainer}>
                                <Text style={styles.queueTitle}>
                                    Single-Drug Urine Test Panel (CD-DBU-114)
                                </Text>
                                <View style={styles.queueQuantity}>
                                    <TouchableHighlight
                                        style={{borderColor: '#dee1e4', borderRightWidth:1,width:25, justifyContent: 'center', alignItems:'center'}}
                                        onPress={this.quantityUp}>
                                        <Text style={{color:'rgb(125, 125, 125)'}}>
                                            +
                                        </Text>
                                    </TouchableHighlight>
                                    <View style={styles.queueQuantityInput}>
                                        <TextInput underlineColorAndroid='transparent' placeholder="0" keyboardType='numeric'>
                                            <Text>
                                                { this.state.count !== 0 ? this.state.count : null}
                                            </Text>
                                        </TextInput>
                                    </View>
                                    <TouchableHighlight
                                        style={{borderColor: '#dee1e4', borderLeftWidth:1, width:25, justifyContent: 'center', alignItems:'center'}}
                                        onPress={this.quantityDown}>
                                        <Text style={{color:'rgb(125, 125, 125)'}}>
                                            -
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                            </View>
                            <View style={styles.loyaltyPointsContainer}>
                                <Text style={styles.loyaltyPointsText}>
                                    You can earn 258 loyalty points per piece.
                                </Text>
                            </View>
                            <TouchableHighlight style={styles.button} onPress={this._toggleModal}>
                                <Text style={styles.buttonText}>BUY IT NOW</Text>
                            </TouchableHighlight>
                            <View style={styles.queueFooter}>
                                <Text style={{bottom:13,color: '#a0a0a0',}}>
                                    Add to favorites
                                </Text>
                                <Image style={{alignItems:'center',marginLeft: 5,bottom:8,marginRight: 10,}}
                                       source={require('../img/ic_favorite_black_24dp.png')}/>
                            </View>
                        </View>
                        <View style={styles.queueContainer}>
                            <TouchableHighlight onPress={() => navigate('QueueScreen')}>
                                <Image style={{alignItems:'center'}} source={require('../img/queue_image.png')}/>
                            </TouchableHighlight>
                            <View style={styles.titleContainer}>
                                <Text style={styles.queueTitle}>
                                    Single-Drug Urine Test Panel (CD-DBU-114)
                                </Text>
                                <View style={styles.queueQuantity}>
                                    <TouchableHighlight
                                        style={{borderColor: '#dee1e4', borderRightWidth:1,width:25, justifyContent: 'center', alignItems:'center'}}
                                        onPress={this.quantityUp}>
                                        <Text style={{color:'rgb(125, 125, 125)'}}>
                                            +
                                        </Text>
                                    </TouchableHighlight>
                                    <View style={styles.queueQuantityInput}>
                                        <TextInput underlineColorAndroid='transparent' placeholder="0" keyboardType='numeric'>
                                            <Text>
                                                { this.state.count !== 0 ? this.state.count : null}
                                            </Text>
                                        </TextInput>
                                    </View>
                                    <TouchableHighlight
                                        style={{borderColor: '#dee1e4', borderLeftWidth:1, width:25, justifyContent: 'center', alignItems:'center'}}
                                        onPress={this.quantityDown}>
                                        <Text style={{color:'rgb(125, 125, 125)'}}>
                                            -
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                            </View>
                            <View style={styles.loyaltyPointsContainer}>
                                <Text style={styles.loyaltyPointsText}>
                                    You can earn 258 loyalty points per piece.
                                </Text>
                            </View>
                            <TouchableHighlight style={styles.button} onPress={this._toggleModal}>
                                <Text style={styles.buttonText}>BUY IT NOW</Text>
                            </TouchableHighlight>
                            <View style={styles.queueFooter}>
                                <Text style={{bottom:13,color: '#a0a0a0',}}>
                                    Add to favorites
                                </Text>
                                <Image style={{alignItems:'center',marginLeft: 5,bottom:8,marginRight: 10,}}
                                       source={require('../img/ic_favorite_black_24dp.png')}/>
                            </View>
                        </View>
                    </View>
                    <Modal isVisible={this.state.isModalVisible}
                           onBackButtonPress={this._toggleModal}
                           onBackdropPress={this._toggleModal}>
                        <View style={styles.modalContainer}>
                            <View style={styles.modalLogo}>
                                <Image style={{alignItems:'center'}} source={require('../img/cartque.jpg')}/>
                            </View>
                            <View style={styles.modalQueueDetails}>
                                <Text style={{margin: 5, flex:1}}>
                                    Single-Drug Urine Test Panel (CD-DBU-114)
                                </Text>
                                <View style={styles.modalPrice}>
                                    <View style={{flexDirection: 'column'}}>
                                        <Text style={styles.modalText}>Price</Text>
                                        <Text style={styles.modalText}>Shipping Price</Text>
                                        <Text style={{borderBottomWidth: 1, fontSize: 10, margin: 5, borderColor: '#0f70b7'}}>Taxes</Text>
                                        <Text style={styles.modalText}>Total Price</Text>
                                    </View>
                                    <View style={{flexDirection: 'column'}}>
                                        <Text style={styles.modalText}>$ 25.77</Text>
                                        <Text style={styles.modalText}>$ 0</Text>
                                        <Text style={{borderBottomWidth: 1, fontSize: 10, margin: 5, borderColor: '#0f70b7'}}>$ 0</Text>
                                        <Text style={styles.modalText}>$ 25.77</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.modalButtons}>
                                <TouchableHighlight style={styles.modalButton}>
                                    <Text style={styles.modalButtonText}>PAY BY INVOICE</Text>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.modalButton}>
                                    <Text style={styles.modalButtonText}>CREDIT CARD PAYMENT</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </Modal>
                </ScrollView>
            </KeyboardAvoidingView>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    queueContainer: {
        margin: 20,
        height:480,
        backgroundColor: '#fafafa',
        shadowOffset:{  width: 10,  height: 10,  },
        shadowColor: 'rgba(121, 121, 121, 0.35)',
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 5,
    },
    titleContainer: {
        margin:10,
        flexDirection: 'row',
    },
    queueTitle: {
        fontWeight: 'bold',
        fontSize:16,
        width:200,
        color: '#2c2c2c'
    },
    button: {
        backgroundColor: '#2daae1',
        alignItems: 'center',
        justifyContent: 'center',
        height:40,
        margin:10,
        marginTop:20,
        borderRadius:5,
    },
    buttonText: {
        color: 'white',
        fontWeight:'bold',
        fontSize:16,
    },
    textInput: {
        width:50,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 0,
    },
    queueQuantity:{
        flexDirection: 'row',
        borderWidth:1,
        borderColor: '#dee1e4',
        borderRadius:6

    },
    loyaltyPointsContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: '#dedede'
    },
    loyaltyPointsText: {
        color: '#a0a0a0',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
    },
    queueFooter: {
        flex:1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginBottom:10,
        marginTop: 10,
        borderTopWidth: 1,
        borderColor: '#dedede'
    },
    modalContainer: {
        backgroundColor: 'white',
        height: 250
    },
    modalLogo: {
        alignItems: 'center',
        marginTop: 15
    },
    modalQueueDetails: {
        margin: 10,
        borderWidth: 1,
        borderColor: '#0f70b7',
        flexDirection: 'row'
    },
    modalPrice: {
        flex:2,
        borderColor: '#ddd',
        backgroundColor: '#f9f9f9',
        borderLeftWidth: 1,
        flexDirection: 'row'
    },
    modalButtons: {
        margin:3,
        flexDirection: 'row',
    },
    modalButton: {
        borderRadius: 5,
        backgroundColor: '#0f70b7',
        alignItems: 'center',
        justifyContent: 'center',
        height:40,
        margin:5,
    },
    modalButtonText: {
        color: 'white',
        fontWeight:'bold',
        marginLeft: 10,
        marginRight:10,
        fontSize: 14,
    },
    modalText: {
        fontSize: 10,
        margin: 5,
    },
    queueQuantityInput: {
        width:50,
        alignItems:'center',
        justifyContent: 'center'
    },
    categoriesContainer: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        marginRight: 20,
    },
    categories: {
        flex: 0.5,
        backgroundColor: '#fafafa',
        shadowOffset:{  width: 10,  height: 10,  },
        shadowColor: 'rgba(121, 121, 121, 0.35)',
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 5,
        margin:5,
        borderRadius: 5
    },
    title: {
        color: '#0f70b7',
        fontWeight: '300',
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 30,
        margin:15
    },
    categoryTitle: {
        color: '#0f70b7',
        fontSize: 14,
        margin: 10,
        marginBottom: 1
    },
    queuesAvailable: {
        margin: 10,
        fontSize:10,
        color: '#2c2c2c',
        marginTop: 1
    }
});
export default HomeScreen;
