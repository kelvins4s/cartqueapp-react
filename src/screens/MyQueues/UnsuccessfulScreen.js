import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import {
    StackNavigator,
} from 'react-navigation';
import Expo from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { Container, Content, Header, Left, Icon } from 'native-base';

class UnsuccessfulScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerStyle: { backgroundColor: '#0f70b7'},
        headerTintColor: 'white',
        headerTitle: (
            <Image style={{alignItems:'center',marginLeft: 60}} source={require('../../img/logo-neg.png')}/>
        ),
    });

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text>
                    No queues found.
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default UnsuccessfulScreen;
