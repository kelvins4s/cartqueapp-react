import React from 'react';
import { Text, View, Button, Image, StyleSheet } from 'react-native';

export default class ProfileScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerStyle: { backgroundColor: '#0f70b7'},
        headerTintColor: 'white',
        headerTitle: (
            <Image style={{alignItems:'center',marginLeft: 60}} source={require('../../img/logo-neg.png')}/>
        ),
    });


    render() {
        return (
            <View style={styles.container}>
                <Image style={{alignItems:'center'}} source={require('../../img/queue_image.png')}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        margin: 10,
    }
})