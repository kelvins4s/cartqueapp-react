import React from 'react';
import { Text, View, Button, Image, StyleSheet, KeyboardAvoidingView, ScrollView, TextInput, TouchableHighlight } from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';


export default class BillingShippingScreen extends React.Component {
    static navigation = {
        tabBarLabel: 'Billing & Shipping'
    };

    render() {
        return (
            <KeyboardAvoidingView style={{flex:1}} behavior="padding" keyboardVerticalOffset={130}>
                <ScrollView style={{backgroundColor: 'white'}}>
                    <Text style={{margin:10, color:'#0e66a6', fontSize:16, borderBottomWidth: 1, borderColor: '#e2e6e9', backgroundColor: 'white'}}>Alerts</Text>
                    <View style={styles.container}>
                        <View style={styles.alerts}>
                            <Text style={styles.title}>Alerts</Text>
                            <View style={styles.radioButtonsContainer}>
                            </View>
                        </View>
                        <View style={styles.frequency}>
                            <Text style={styles.title}>Frequency</Text>

                        </View>
                        <TouchableHighlight style={styles.button}>
                            <Text style={styles.buttonText}>SAVE</Text>
                        </TouchableHighlight>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    alerts: {
        margin: 10,
        marginBottom:20,
    },
    frequency: {
        margin: 10,
    },
    title: {
        color: '#2daae1',
        fontSize: 24,
    },
    text:{
        marginLeft: 10,
        marginRight: 10,
        marginBottom:10,
        color: '#2c2c2c',
    },
    input: {
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        height:40,
        borderColor: '#dcdcdc',
        borderRadius:2,
        borderWidth: 1,
        padding:10,
        width: '90%',
        backgroundColor: 'white'
    },
    button: {
        borderRadius: 5,
        backgroundColor: '#0f70b7',
        alignItems: 'center',
        justifyContent: 'center',
        height:40,
        margin:10,
    },
    buttonText: {
        color: 'white',
        fontWeight:'bold',
        marginLeft: 20,
        marginRight:20,
        fontSize: 14,
    },
    radioButtonsContainer: {
        margin: 10,
    }
});