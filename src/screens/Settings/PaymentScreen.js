import React from 'react';
import { Text, View, Button, Image, StyleSheet, ScrollView, TextInput, KeyboardAvoidingView, TouchableHighlight } from 'react-native';

export default class PaymentScreen extends React.Component {
    static navigation = {
        tabBarLabel: 'Payment'
    };
    render() {
        return (
            <KeyboardAvoidingView style={{flex:1}} behavior="padding" keyboardVerticalOffset={64}>
                <ScrollView style={{backgroundColor: 'white'}}>
                    <Text
                        style={{margin:10, color:'#0e66a6', fontSize:16, borderBottomWidth: 1, borderColor: '#e2e6e9', backgroundColor: 'white'}}>Payment</Text>
                    <View style={styles.container}>
                        <View style={{margin:20}}>
                            <Text style={{fontSize:20}}>Card Number:</Text>
                            <Text>XXXX-XXXX-XXXX-4242</Text>
                        </View>
                        <View>
                            <TouchableHighlight style={styles.button}>
                                <Text style={styles.buttonText}>CHANGE CARD</Text>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.button}>
                                <Text style={styles.buttonText}>REMOVE CARD</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        elevation: 0,
    },
    settingsProfileTop: {
        margin: 10,
        backgroundColor: '#f5f8fa',
    },
    profileImage: {
        height: 150,
        width: 150,
        backgroundColor: 'white',
        alignSelf: 'center',
    },
    loyaltyPointsText: {
        color: '#0f70b7',
        alignSelf: 'center'
    },
    profileText:{
        marginLeft: 10,
        marginRight: 10,
        marginBottom:10,
        color: '#2c2c2c',
    },
    profileInput: {
        marginLeft: 10,
        marginRight: 10,
        height:40,
        borderColor: '#dcdcdc',
        borderRadius:2,
        borderWidth: 1,
        padding:10,
        width: '90%',
        backgroundColor: 'white'
    },
    dropdownContainer: {
        marginBottom: 20,
        margin: 10,
    },
    dropdown: {
        margin: 10,
        height:40,
    },
    button: {
        borderRadius: 5,
        backgroundColor: '#0f70b7',
        alignItems: 'center',
        justifyContent: 'center',
        height:40,
        margin:10,
    },
    buttonText: {
        color: 'white',
        fontWeight:'bold',
        marginLeft: 10,
        marginRight:10,
        fontSize: 14,
    },
});