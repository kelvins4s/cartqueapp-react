import React from 'react';
import { Text, View, Button, Image, StyleSheet, ScrollView, TextInput, KeyboardAvoidingView, TouchableHighlight } from 'react-native';
import { Container, Content, Header, Body, Left, Icon } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';

export default class ProfileScreen extends React.Component {
    static navigation = {
        tabBarLabel: 'Profile',
    };

    render() {
        let classOfTrade = [{
            value: 'Physician Office',
        }, {
            value: 'Surgery Center',
        }, {
            value: 'Long Term Care',
        }, {
            value: 'Home Health',
        }, {
            value: 'Long Term Acute Care',
        }, {
            value: 'Dental',
        }, {
            value: 'FHQC',
        }, {
            value: 'Rural Health'
        }];

        let title = [{
            value: 'Physician',
        }, {
            value: 'Nurse',
        }, {
            value: 'CEO',
        }, {
            value: 'C-Suite',
        }, {
            value: 'Administrator',
        }];
        return (
            <KeyboardAvoidingView style={{flex:1}} behavior="padding" keyboardVerticalOffset={130}>
                <ScrollView style={{backgroundColor: 'white'}}>
                    <Text
                        style={{margin:10, color:'#0e66a6', fontSize:16, borderBottomWidth: 1, borderColor: '#e2e6e9', backgroundColor: 'white'}}>Profile</Text>
                    <View style={styles.container}>
                            <View>
                                <Image
                                    style={styles.profileImage}
                                    source={require('../../img/profile-image.png')}
                                />
                                <Text style={styles.loyaltyPointsText}>51552 Loyalty Points</Text>
                                <Text style={styles.profileText}>Name</Text>
                                <TextInput underlineColorAndroid='transparent' value="Narcis Macinoi"
                                           style={styles.profileInput}/>
                                <Text style={styles.profileText}>Email</Text>
                                <TextInput underlineColorAndroid='transparent' value="narcis.macinoi@gmail.com"
                                           style={styles.profileInput} keyboardType='email-address'/>
                            </View>
                        <View>
                            <View>
                                <View style={styles.dropdownContainer}>
                                    <Dropdown
                                        label="Select class of trade"
                                        data={classOfTrade}
                                        style={styles.dropdown}
                                        baseColor="#0f70b7"
                                    />
                                    <Dropdown
                                        label="Select title"
                                        data={title}
                                        style={styles.dropdown}
                                        baseColor="#0f70b7"
                                    />
                                </View>
                                <Text style={styles.profileText}>Old Password</Text>
                                <TextInput underlineColorAndroid='transparent'
                                           style={styles.profileInput}/>
                                <Text style={styles.profileText}>New password</Text>
                                <TextInput underlineColorAndroid='transparent'
                                           style={styles.profileInput}/>
                                <Text style={styles.profileText}>Confirm password</Text>
                                <TextInput underlineColorAndroid='transparent'
                                           style={styles.profileInput}/>
                                <TouchableHighlight style={styles.button}>
                                    <Text style={styles.buttonText}>CHANGE</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        elevation: 0,
    },
    settingsProfileTop: {
        margin: 10,
        backgroundColor: '#f5f8fa',
    },
    profileImage: {
        height: 150,
        width: 150,
        backgroundColor: 'white',
        alignSelf: 'center',
    },
    loyaltyPointsText: {
        color: '#0f70b7',
        alignSelf: 'center'
    },
    profileText:{
        marginLeft: 10,
        marginRight: 10,
        marginBottom:10,
        color: '#2c2c2c',
    },
    profileInput: {
        marginLeft: 10,
        marginRight: 10,
        height:40,
        borderColor: '#dcdcdc',
        borderRadius:2,
        borderWidth: 1,
        padding:10,
        width: '90%',
        backgroundColor: 'white'
    },
    dropdownContainer: {
        marginBottom: 20,
        margin: 10,
    },
    dropdown: {
        margin: 10,
        height:40,
    },
    button: {
        borderRadius: 5,
        backgroundColor: '#0f70b7',
        alignItems: 'center',
        justifyContent: 'center',
        height:40,
        margin:10,
    },
    buttonText: {
        color: 'white',
        fontWeight:'bold',
        marginLeft: 10,
        marginRight:10,
        fontSize: 14,
    },
});