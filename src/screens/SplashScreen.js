import React from 'react';
import { Text, View, StyleSheet, Image, TouchableHighlight } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Content, Header, Left, Icon } from 'native-base';
import { NavigationActions } from 'react-navigation'

const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'HomeScreen'})
    ]
})


class SplashScreen extends React.Component {
    static navigationOptions = {
        header:null
    };
    componentWillMount() {
        setTimeout(()=>{
            this.props.navigation.dispatch(resetAction)
            // this.props.navigation.navigate('HomeScreen');
        },3000)
    }
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.logo} source={require('../../img/cartque_logo.jpg')} />
                <Image style={styles.spinner} source={require('../../img/loading_spinner.gif')} />
                <Text style={styles.footer}>Copyright CartQue 2018</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
    },
    header: {
        color: 'black',
        fontSize: 40,
    },
    spinner: {
        marginTop: 10,
        height: 60,
        width: 60,
        borderColor: '#2daae1',
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        alignItems: 'center'
    },
})

export default SplashScreen;