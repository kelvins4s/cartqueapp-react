import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import {
    StackNavigator,
} from 'react-navigation';
import Expo from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { Container, Content, Header, Left, Icon } from 'native-base';

class MyOrdersScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerLeft: (
            <Icon name="md-menu" size={28} style={{paddingLeft: 20, color: 'white'}} onPress={() => navigation.navigate('DrawerOpen')}/>
        ),
        headerStyle: { backgroundColor: '#0f70b7'},
        headerTitle: (
            <Image style={{alignItems:'center',marginLeft: 60}} source={require('../../img/logo-neg.png')}/>
        ),
    });

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Content contentContainerStyle={{
                    flex:1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Text>asd</Text>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default MyOrdersScreen;