import React from 'react';
import { Text, View, StyleSheet, Button, TouchableHighlight, Image } from 'react-native';
import {
    StackNavigator,
} from 'react-navigation';
import Expo from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { Container, Content, Header, Left, Icon } from 'native-base';
import ProcessingScreen from './MyQueues/ProcessingScreen';
import UnsuccessfulScreen from './MyQueues/UnsuccessfulScreen';
import CompleteScreen from './MyQueues/CompleteScreen';
import DiscountedScreen from './MyQueues/DiscountedScreen';
import WatchingScreen from './MyQueues/WatchingScreen';
import ShippingScreen from './MyQueues/ShippingScreen';
import ActiveScreen from './MyQueues/ActiveScreen';

class MyQueuesScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerLeft: (
            <Icon name="md-menu" size={28} style={{paddingLeft: 20, color: 'white'}} onPress={() => navigation.navigate('DrawerOpen')}/>
        ),
        headerStyle: { backgroundColor: '#0f70b7'},
        headerTitle: (
            <Image style={{alignItems:'center',marginLeft: 60}} source={require('../../img/logo-neg.png')}/>
        ),
    });

    render() {
        const { navigate } = this.props.navigation;
        return (

        <View style={styles.container}>
            <View>
                <Text style={{margin:10, color: '#0e66a6', fontSize:16}}>My Queues</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'column'}}>
                <TouchableHighlight style={styles.button} onPress={() => navigate('ActiveScreen')}>
                    <Text style={styles.text}>
                        Active(0)
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.button} onPress={() => navigate('ProcessingScreen')}>
                    <Text style={styles.text}>
                        Processing(0)
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.button} onPress={() => navigate('UnsuccessfulScreen')}>
                    <Text style={styles.text}>
                        Unsuccessful(0)
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.button} onPress={() => navigate('CompleteScreen')}>
                    <Text style={styles.text}>
                        Complete(0)
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.button} onPress={() => navigate('DiscountedScreen')}>
                    <Text style={styles.text}>
                        Discounted(0)
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.button} onPress={() => navigate('ShippingScreen')}>
                    <Text style={styles.text}>
                        Shipping(0)
                    </Text>
                </TouchableHighlight>
            </View>
        </View>
        )
    }
}




const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'white',
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#0f70b7',
        padding: 10,
        marginTop: 2,
        marginBottom: 2,
        borderRadius: 5,
        justifyContent: 'center',
        marginRight:10,
        marginLeft: 10,
    },
    text: {
        color: 'white',
        fontSize: 16,
    }
})

export default MyQueuesScreen;
