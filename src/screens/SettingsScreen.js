import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import {
    TabNavigator
} from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import { Container, Content, Header, Left, Icon } from 'native-base';

import ProfileScreen from './Settings/ProfileScreen';
import BillingShippingScreen from './Settings/BillingShippingScreen';
import AlertsScreen from './Settings/AlertsScreen';
import PaymentScreen from './Settings/PaymentScreen';

class SettingsScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerLeft: (
            <Icon name="md-menu" size={28} style={{paddingLeft: 20, color: 'white'}} onPress={() => navigation.navigate('DrawerOpen')}/>
        ),
        headerStyle: { backgroundColor: '#0f70b7'},
        headerTitle: (
            <Image style={{alignItems:'center',marginLeft: 60}} source={require('../../img/logo-neg.png')}/>
        ),
    });

    render() {
       return <SettingsNavigator />
    }
}

const SettingsNavigator = TabNavigator({
    Profile: {
        screen: ProfileScreen,
        navigationOptions: {
            tabBarIcon: () => <Image source={require('../../img/ic_face_black_24dp.png')}/>
        }
    },
    Addresses: {
        screen: BillingShippingScreen,
        navigationOptions: {
            tabBarIcon: () => <Image source={require('../../img/ic_place_black_24dp.png')}/>
        }
    },
    Payment: {
        screen: PaymentScreen,
        navigationOptions: {
            tabBarIcon: () => <Image source={require('../../img/ic_credit_card_black_24dp.png')}/>
        }
    },
    Alerts: {
        screen: AlertsScreen,
        navigationOptions: {
            tabBarIcon: () => <Image source={require('../../img/ic_add_alert_black_24dp.png')}/>
        }
    }
    }, {
    tabBarOptions: {
        animationEnable: true,
        showIcon: true,
        showLabel: false,
        lazy: true,
        indicatorStyle : {
            backgroundColor: '#0f70b7',
        },
        style: {
            backgroundColor: 'white',
            borderBottomWidth: 1,
            borderColor: '#2daae1'
        },
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default SettingsScreen;

