import React from 'react';
import { Text, View, StyleSheet, Image, TouchableHighlight } from 'react-native';
import {
    StackNavigator,
    DrawerNavigator,
    DrawerItems,
} from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import { Container, Content, Header, Body, Left, Icon } from 'native-base';
import SplashScreen from '../screens/SplashScreen';
import HomeScreen from '../screens/HomeScreen'
import MyQueuesScreen from '../screens/MyQueuesScreen';
import MyOrdersScreen from '../screens/MyOrdersScreen';
import SettingsScreen from '../screens/SettingsScreen';
import AdvancedSearchScreen from '../screens/AdvancedSearchScreen';
import LogoutScreen from '../screens/LogoutScreen';
import ActiveScreen from '../screens/MyQueues/ActiveScreen';
import CompleteScreen from '../screens/MyQueues/CompleteScreen';
import DiscountedScreen from '../screens/MyQueues/DiscountedScreen';
import ProcessingScreen from '../screens/MyQueues/ProcessingScreen';
import ShippingScreen from '../screens/MyQueues/ShippingScreen';
import UnsuccessfulScreen from '../screens/MyQueues/UnsuccessfulScreen';
import WatchingScreen from '../screens/MyQueues/WatchingScreen';
import QueueScreen from '../screens/Queue/QueueScreen';

const CustomDrawerContentComponent = (props) => (
    <Container>
        <Header style={{ height: 200, backgroundColor: '#0f70b7', padding: 0, margin:0}}>
            <Body>
            <Image
                style={styles.drawerImage}
                source={require('../img/profile-image.png')}
            />
            </Body>
        </Header>
        <Content style={{backgroundColor:'#0f70b7'}}>
            <DrawerItems {...props}  activeTintColor='#FFFFFF' activeBackgroundColor='#2daae1' inactiveTintColor='#FFFFFF' inactiveBackgroundColor='#0f70b7' style={{paddingBottom: 10, backgroundColor: '#2daae1'}}/>
        </Content>
    </Container>
);

const HomePage = StackNavigator({
    // SplashScreen: {
    //     screen: SplashScreen,
    // },
    HomeScreen: {
        screen: HomeScreen,
    },
    QueueScreen: {
        screen: QueueScreen,
    }
});

const MyQueuesPages = StackNavigator({
    MyQueuesScreen: {
        screen: MyQueuesScreen,
    },
    ActiveScreen:{
        screen: ActiveScreen,
    },
    CompleteScreen:{
        screen: CompleteScreen,
    },
    DiscountedScreen:{
        screen: DiscountedScreen,
    },
    ProcessingScreen:{
        screen: ProcessingScreen,
    },
    ShippingScreen:{
        screen: ShippingScreen,
    },
    UnsuccessfulScreen:{
        screen: UnsuccessfulScreen,
    },
    WatchingScreen:{
        screen: WatchingScreen,
    }
});

const MyOrdersPage = StackNavigator({
    MyOrdersScreen: {
        screen: MyOrdersScreen,
    },
});

const SettingsPage = StackNavigator({
    SettingsScreen: {
        screen: SettingsScreen,
    },
});

const AdvancedSearchPage = StackNavigator({
    AdvancedSearchScreen: {
        screen: AdvancedSearchScreen,
    },
});

const LogoutPage = StackNavigator({
    LogoutScreen: {
        screen: LogoutScreen,
    },
});

const Drawer = DrawerNavigator({
    Home: {
        screen: HomePage,
        navigationOptions: {
            drawer: {
                label: 'Home',
            },
            drawerIcon: (
                <Icon name="md-home" style={{color: '#FFFFFF'}}/>
            ),
        }
    },
    MyQueues: {
        screen: MyQueuesPages,
        navigationOptions: {
            drawer: {
                label: 'My Queues',
            },
            drawerIcon: (
                <Icon name="md-heart" style={{color: '#FFFFFF'}}/>
            ),
        }
    },
    MyOrders: {
        screen: MyOrdersPage,
        navigationOptions: {
            drawer: {
                label: 'My Orders',
            },
            drawerIcon: (
                <Icon name="md-cart" style={{color: '#FFFFFF'}}/>
            ),
        }
    },
    Settings: {
        screen: SettingsPage,
        navigationOptions: {
            drawer: {
                label: 'Settings',
            },
            drawerIcon: (
                <Icon name="md-settings" style={{color: '#FFFFFF'}}/>
            ),
        }
    },
    AdvancedSearch: {
        screen: AdvancedSearchPage,
        navigationOptions: {
            drawer: {
                label: 'Advanced Search',
            },
            drawerIcon: (
                <Icon name="md-search" style={{color: '#FFFFFF'}}/>
            ),
        }
    },
    Logout: {
        screen: LogoutPage,
        navigationOptions: {
            drawer: {
                label: 'Logout',
            },
            drawerIcon: (
                <Icon name="md-lock" style={{color: '#FFFFFF'}}/>
            ),
        }
    }
}, {
    contentOption: {
        activeBackgroundColor: '#0f70b7',
        inactiveBackgroundColor: '#0f70b7'
    },
    initialRouteName: 'Home',
    contentComponent: CustomDrawerContentComponent,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle'
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    splashscreenContainer:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    splashscreenHeader: {
        color: 'black',
        fontSize: 40,
    },
    splashscreenImage: {
        height: 60,
        width: 60,
    },
    drawerImage: {
        height: 150,
        width: 150,
        backgroundColor: '#0f70b7',
        alignSelf: 'center',
    }
});
export default Drawer;